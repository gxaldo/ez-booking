package com.example.android.myapplication;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by aldo on 24/05/2016.
 */
public class OneAdapter extends RecyclerView.Adapter<OneAdapter.MyViewHolder> {
    private ArrayList<DataModel> dataSet;
    private static Context context;
    private static LinearLayout jadwal;
    private static Button detail;
    private static RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private static RecyclerView recyclerView;
    public OneAdapter(Context context, ArrayList<DataModel> dataSet) {
        this.context = context;
        this.dataSet = dataSet;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

//        TextView txtName;


        ImageView imgView;



        public MyViewHolder(final View itemView) {
            super(itemView);
//            this.txtName = (TextView) itemView.findViewById(R.id.like);


            jadwal = (LinearLayout)itemView.findViewById(R.id.button_jadwal);
            jadwal.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(view.getContext(), JadwalBook.class);
                    view.getContext().startActivity(i);
                }

            });

////            detail = (Button)itemView.findViewById(R.id.button_detail);
////            detail.setOnClickListener(new View.OnClickListener() {
////                @Override
////                public void onClick(View view) {
////                    Intent i = new Intent(view.getContext(), JadwalBook.class);
////                    view.getContext().startActivity(i);
////                }
//
//            });
        }
    }

    public OneAdapter(ArrayList<DataModel> data) {
        this.dataSet = data;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_layout, parent, false);

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {
//        TextView txtName = holder.txtName;
//
//
//
//        txtName.setText(dataSet.get(listPosition).getName());




    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


}
