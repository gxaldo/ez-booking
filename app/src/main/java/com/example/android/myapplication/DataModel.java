package com.example.android.myapplication;

/**
 * Created by User on 4/4/2016.
 */
public class DataModel {

    String name;
    String image;

    public DataModel(String name, String date, String image, String hour, String comment) {
        this.name = name;
        this.date = date;
        this.hour = hour;
        this.image = image;
        this.comment = comment;
    }

    String date;
    String hour;
    String comment;

    public String getImage() {
        return image;
    }

    public String getDate() {
        return date;
    }

    public String getHour() {
        return hour;
    }

    public String getComment() {
        return comment;
    }

    public String getName() {
        return name;
    }

}